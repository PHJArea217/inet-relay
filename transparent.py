#!/usr/bin/env python3
import socket
import os
# ip6tables -t mangle -A PREROUTING -d 64:ff9b::/96 -p tcp -m multiport --dports 80,443 -j TPROXY --on-ip ::1 --on-port 1
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(41, 75, 1) # SOL_IPV6, IPV6_TRANSPARENT

s.bind(('::1', 1))
s.set_inheritable(1);
os.execv("/bin/sh", ["/bin/sh", "-c", 'exec setpriv --no-new-privs --reuid=1000 --regid=1000 --clear-groups ./inet-relay -c::ffff:0:0 -p0 -l"$1" -N', "-", str(s.fileno())])
