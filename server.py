#!/usr/bin/env python3
import socket
import os
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('::', 80))
s.set_inheritable(1);
os.execv("/bin/sh", ["/bin/sh", "-c", 'exec setpriv --no-new-privs --reuid=1000 --regid=1000 --clear-groups ./inet-relay -c::ffff:0:0 -l"$1" -N', "-", str(s.fileno())])
