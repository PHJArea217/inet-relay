inet-relay: freeform relaying for supporting IPv6 deployments.

Useful as:

* Allowing non-link-local-aware software to connect to link local addresses
* NAT64-compatible gateway (TCP only)
* Tunneling connections between network namespaces
* Since the socket is specified only by a file descriptor number (there is no code to bind a socket), there can be great flexibility as to the nature of the listening socket. For example, the socket could be in a different network namespace, the socket can be created before dropping CAP_NET_BIND_SERVICE (changing to unprivileged user), certain socket options could be specified manually, etc. Also friendly with the iptables TPROXY module.

Todo:
* Support /64 in addition to /96 for NAT64 mode
* Support IPv4 listening socket (for 464XLAT support)
* Arbitrary network mappings, perhaps via a module that reads the local and remote IP addresses and computes the necessary socket parameters to create
* Code to first create the listening socket in a user namespace while still retaining privileges in the original network namespace
* Randomization of source IP address
* Relaying between any two sockets specified by file descriptors (sort of like the "inetd mode" of this program)
* UDP

Technically this project would be called "Internet Relay", but "inet-relay" was chosen due to confusion with "Internet Relay Chat" and that "inet" is already a common abbreviation for "Internet" in the socket() syscall.

License: MIT
