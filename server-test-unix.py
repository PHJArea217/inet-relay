#!/usr/bin/env python3
import socket
import os
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('::', 8080))
s.set_inheritable(1);
os.execv("/bin/sh", ["/bin/sh", "-c", 'exec setpriv --no-new-privs ./inet-relay -u /run/user/1000/inet-relay-test/socket -l"$1" -N', "-", str(s.fileno())])
